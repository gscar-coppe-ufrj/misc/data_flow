#include <Dataflow/BaseInput.h>
#include <Dataflow/Node.h>

#include <iostream>

using namespace Dataflow;

class BaseInput::Impl 
{
public:
    Impl() : portNumber(0) {};
	const BaseSignal* value_;
    BaseOutput* source_;
    const Node* owner;
    unsigned int portNumber;
};

BaseInput::BaseInput() : d_ptr(new Impl) 
{
	d_ptr->value_ = 0;
	d_ptr->source_ = 0;
}

BaseInput::BaseInput(const Node* node) : d_ptr(new Impl)
{
    d_ptr->owner = node;
}

unsigned int BaseInput::getNodeId()
{
    return d_ptr->owner->getId();
}

unsigned int BaseInput::getPortNumber()
{
    return d_ptr->portNumber;
}

void BaseInput::setPortNumber(unsigned int n)
{
    d_ptr->portNumber = n;
}

BaseInput::~BaseInput() 
{
	delete d_ptr;
}

void BaseInput::bind(const BaseSignal* signal) 
{
	d_ptr->value_ = signal;
}

const BaseSignal* BaseInput::value() const 
{
	return d_ptr->value_;
}

void BaseInput::connect(BaseOutput* output) 
{
	d_ptr->source_ = output;
}

void BaseInput::disconnect() 
{
	//source_->disconnect(this);
	d_ptr->source_ = 0;
};

