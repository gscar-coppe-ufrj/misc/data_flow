#include <Dataflow/Dataflow.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp> 

#include <iostream>
#include <vector>

using namespace Dataflow;
using namespace std;
using boost::shared_ptr;

class NumericSignal : public BaseSignal
{
public:
    NumericSignal(double v) : v_(v) {};
    NumericSignal() : v_(0L) {};
    
    virtual ~NumericSignal() {};
    
    virtual BaseSignal& operator=(const BaseSignal* rhs) {
    	const NumericSignal* a = dynamic_cast<const NumericSignal*>(rhs);
		if (a != 0)
            v_ = a->v_;
        return *this;
    };
    
    double v_;
};

class ConstBlock : public Node
{
public:
    ConstBlock() : Node(0,1) {
        setOutputType(0, new Output<NumericSignal>());
    }
    
    virtual void setup() {
        BaseOutput& out = output(0);
        NumericSignal v(1L);
        //if (out == 0)
        //    cout << "O_O\n";
        out.set(&v);
    }
    
    virtual const char* name() {
        return "Sources::Constant";
    }
    
    virtual void process() {
    
    }
};

class Sum : public Node
{
public:
    Sum() : Node(2,1) {
        setInputType(0, new Input<NumericSignal>(this));
        setInputType(1, new Input<NumericSignal>(this));
        //output(0) = new Output<NumericSignal>();
        setOutputType(0, new Output<NumericSignal>());
    }
    
    virtual void setup() {
        NumericSignal v(0L);
        output(0).set(&v);
    }
    
    virtual void process() {
        const NumericSignal* A = input(0).value<NumericSignal>();
        const NumericSignal* B = input(1).value<NumericSignal>();
        
        NumericSignal result(A->v_ - B->v_);
        output(0).set(&result);
    }
    
    virtual const char* name() {
        return "Math::Sum";
    }
};

class Integrator : public Node
{
public:
    Integrator() : Node(1,1) {
        currTime = 0.0f;
        lastTime = 0.0f;
        
        setInputType(0, new Input<NumericSignal>(this));
        setOutputType(0, new Output<NumericSignal>());
    };
    
    virtual void setup() {
        NumericSignal n(0L);
        currValue = 0L;
        BaseOutput& out = output(0);
        //if (out != 0)
            out.set(&n);
        //else
        //    cout << "O_O\n";
    }
    
    virtual const char* name() {
        return "Continuous::Integrator";
    }
    
    virtual void process() {
        currTime += 0.001f;
            
        const NumericSignal* in = input(0).value<NumericSignal>();
        BaseOutput& out = output(0);
        
        float deltaTime = currTime - lastTime;
        double value = deltaTime * in->v_;
        currValue += value;
        cout << currTime << "," << currValue << endl;
        
        NumericSignal n(currValue);
        //if (out == 0) 
        //    cout << "o_O\n";
        out.set(&n);
        
        lastTime = currTime;
    }
    
    float currTime;
    float lastTime;
    double currValue;
};

int main(int argc, char** argv)
{
    Integrator i;
    ConstBlock cb;
    Sum s;
    Sum s2;
    
    i.setup();
    cb.setup();
    s.setup();
    s2.setup();

    //cb.output(0).connect(s.input(0));
    //i.output(0).connect(s.input(1));
    //s.output(0).connect(i.input(0));
    
    cb.output(0) >>= s.input(0);
    i.output(0) >>= s.input(1);
    s.output(0) >>= i.input(0);
    cb.output(0) >>= s2.input(0);
    cb.output(0) >>= s2.input(1);
    
    int loopCount = atoi(argv[1]);
    
    for (int j = 0; j < loopCount; j++) {
        s.process();
        i.process();
    }
    
    System sys;
    sys.addNode(&cb);
    sys.addNode(&i);
    sys.addNode(&s);
    sys.addNode(&s2);
    
    sys.save();
    
    return 0;
}
