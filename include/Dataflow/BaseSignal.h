#ifndef BASESIGNAL_H
#define BASESIGNAL_H

namespace Dataflow {

/*!
 * \brief Defines a signal that can be passed between nodes.
 */
class BaseSignal {
public:
    /*!
     * \brief Default destructor.
     */
    virtual ~BaseSignal();
    
    /*!
     * \brief Operator =.
     * \param rhs Right hand side of operator.
     */
    virtual BaseSignal& operator=(const BaseSignal* rhs) = 0;
};

}; // namespace Dataflow

#endif // BASESIGNAL_H
