#include "BaseOutput.h"

#ifndef OUTPUT_H
#define OUTPUT_H

namespace Dataflow {

/*!
 * \brief Defines an output port with a signal type.
 * \tparam T The type that this output will produce. Should be derived from
 *      Dataflow::BaseSignal.
 */
template <class T>
class Output : public BaseOutput {
public:
    /*!
     * \brief Default constructor.
     */
    Output() : BaseOutput(new T) {};
}; // class Output

}; // namespace Dataflow

#endif // OUTPUT_H
