#include "BaseInput.h"
#include "BaseOutput.h"

#include <boost/shared_ptr.hpp>

#include <cstddef> // size_t

#ifndef NODE_H
#define NODE_H

namespace Dataflow {

/*!
 * \brief This is the base class for a node.
 * 
 */
class Node
{
public:
    Node();
    Node(std::size_t inputs, std::size_t outputs);

    ~Node();

    // Port access
    BaseOutput& output(std::size_t i);
    BaseInput& input(std::size_t i);

    // Port information
    unsigned int inputCount();
    unsigned int outputCount();
    
    // Node operation related
    virtual void setup();
    virtual void process() = 0;

    // System model functions
    void setId(const unsigned int id);
    const unsigned int getId() const;
    virtual const char* name() = 0;

protected:
    void setInputType(std::size_t pos, BaseInput* input);
    void setOutputType(std::size_t pos, BaseOutput* output);

    void addInput(BaseInput* input);

    void addOutput(BaseOutput* output);

private:
    /*!
     * \brief Implementation class.
     */
    class Impl;
    /// Pimpl.
    Impl* d_ptr;
};

}; // namespace Dataflow

#endif // NODE_H
