#include "Node.h"
#include "BaseInput.h"
#include "BaseOutput.h"
#include "BaseSignal.h"

#ifndef SYSTEM_H
#define SYSTEM_H

namespace Dataflow {
class System {
public:
    System();
    ~System();
    
    void addNode(Dataflow::Node* node);
    
    void save();
    
private:
    class Impl;
    Impl* d_ptr;
};

}; // namespace Dataflow

#endif // SYSTEM_H
