#include "BaseInput.h"
#include "BaseSignal.h"

#ifndef INPUT_H
#define INPUT_H

namespace Dataflow {

/*!
 * \brief Defines an input port with a signal type.
 * \tparam T The class that this input accepts as input signal. Should be 
 *      derived from Dataflow::BaseSignal.
 */
template <class T>
class Input : public BaseInput {
public:
    /*!
     * \brief Default constructor.
     */
    Input() {};

    /*!
     * \brief Construct this input as a child of a node.
     * \param[in] node The parent node of this input.
     */
    Input(const Node* node) : BaseInput(node) {};

    /*!
     * \brief Default destructor.
     */
    virtual ~Input() {};

    /*!
     * \brief Checks if a signal can be connected to this input. Checks if the
     *      signal can be dynamic cast to the input's type.
     * \param[in] signal The signal to check.
     * \return True if the signal can connect, false otherwise.
     */
    virtual bool check(const BaseSignal* signal) const {
        if (dynamic_cast<const T*>(signal) == nullptr)
            return false;
        return true;
    }
   
}; // class Input

}; // namespace Dataflow

#endif // INPUT_H
