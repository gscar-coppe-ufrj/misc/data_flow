#include "BaseSignal.h"

#ifndef BASEINPUT_H
#define BASEINPUT_H

namespace Dataflow {

// Forward declaration of class Node
class Node;

// Forward declaration of class BaseOutput
class BaseOutput;

/*!
 * \brief Defines a basic input port.
 */
class BaseInput 
{
public:
	/*!
	 * \brief Default constructor.
	 */
	BaseInput();
	
	BaseInput(const Node*);
	
	unsigned int getNodeId();
	
	void setPortNumber(unsigned int n);
	unsigned int getPortNumber();
	
	/*!
	 * \brief BaseInput is a polymorphic class.
	 */
    virtual ~BaseInput();

	/*!
	 * \brief
	 * \param[in] signal The signal to check.
	 * \return True if signal can be dynamic cast to the input type.
	 */
    virtual bool check(const BaseSignal* signal) const = 0;
    
    /*!
     * \brief Binds the internal value to a signal.
     * \param[in] signal The signal to bind to.
     */
    virtual void bind(const BaseSignal* signal);
	
	/*!
	 * \brief Get the value of the input.
	 * \return The value of the input.
	 */
	const BaseSignal* value() const;

	/*!
	 * \brief Get the value of the input dynamic casted to U.
	 * \tparam U The class to cast to.
	 * \return The value of the input dynamic casted to U.
	 */
    template <class U>
    const U* value() const {
		return dynamic_cast<const U*>(value());
    }

    void connect(BaseOutput* output);
    
    void disconnect();
    
private:
    BaseInput(BaseInput&) {};
    BaseInput& operator=(BaseInput& other) {};
	class Impl;
	Impl* d_ptr;
};

}; // namespace Dataflow
#endif
