#include "BaseSignal.h"

#include <vector>

#ifndef BASEOUTPUT_H
#define BASEOUTPUT_H

namespace Dataflow {

class BaseInput;

struct ConnectionInfo {
    unsigned int count;
    
    std::vector<unsigned int> nodeId;
    std::vector<unsigned int> port;
};

class BaseOutput 
{
public:
	BaseOutput(BaseSignal* type);

    virtual ~BaseOutput();

    void connect(BaseInput* base);

    void set(const BaseSignal* rhs);
    
    ConnectionInfo connectionInfo();

    /*!
	 * \brief Get the value of the output.
	 * \return The value of the output.
	 */
	const BaseSignal* value() const;

    /*!
	 * \brief Get the value of the output dynamic casted to U.
	 * \tparam U The class to cast to.
	 * \return The value of the output dynamic casted to U.
	 */
    template <class U>
    const U* value() const {
		return dynamic_cast<const U*>(value());
    };

    BaseOutput& operator>>=(BaseInput& rhs) {
        connect(&rhs);
        return *this;
    };

private:
	class Impl;
	Impl* d_ptr;
};

}; // namespace Dataflow

#endif // BASEOUTPUT_H
