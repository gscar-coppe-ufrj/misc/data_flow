#include <Dataflow/Node.h>
#include <Dataflow/BaseInput.h>
#include <Dataflow/BaseSignal.h>
#include <Dataflow/Input.h>

#include <memory>
#include <vector>

using namespace Dataflow;
using namespace std;

class Node::Impl
{
public:
    Impl() : id_(0) {};
    
    ~Impl() {
        _input.clear();
        _output.clear();
    }
    std::vector< std::shared_ptr<BaseInput> > _input;
    std::vector< std::shared_ptr<BaseOutput> > _output;
    
    unsigned int id_;
};

void Node::setId(const unsigned int id)
{
    d_ptr->id_ = id;
}

const unsigned int Node::getId() const
{
    return d_ptr->id_;
}

unsigned int Node::inputCount() 
{
    return d_ptr->_input.size();
}

unsigned int Node::outputCount()
{
    return d_ptr->_output.size();
}

BaseOutput& Node::output(size_t i)
{
    //if (i >= d_ptr->_output.size())
        //return NULL;
    return *(d_ptr->_output.at(i).get());
}

BaseInput& Node::input(size_t i)
{
	//if (i >= d_ptr->_input.size())
	//	return NULL;
    return *(d_ptr->_input.at(i).get());
}
/*
BaseInput* Node::input(size_t i) const
{
	if (i >= d_ptr->_input.size())
		return NULL;
    return d_ptr->_input[i].get();
}
*/
/*
BaseOutput* Node::output(size_t i) const
{
    if (i >= d_ptr->_output.size())
        return NULL;
    return d_ptr->_output[i].get();
}
*/
Node::Node() : d_ptr(new Impl)
{
}

Node::~Node()
{
	delete d_ptr;
}

Node::Node(size_t inputs, size_t outputs) : d_ptr(new Impl)
{
	d_ptr->_input.resize(inputs);
	d_ptr->_output.resize(outputs);
}

void Node::addInput(BaseInput* input)
{
	d_ptr->_input.push_back(std::shared_ptr<BaseInput>(input));
}

void Node::addOutput(BaseOutput* output)
{
	d_ptr->_output.push_back(std::shared_ptr<BaseOutput>(output));
}

void Node::setup()
{
}

void Node::setInputType(std::size_t pos, BaseInput* input)
{
	if (pos >= d_ptr->_input.size())
		return;
	
	d_ptr->_input[pos] = std::shared_ptr<BaseInput>(input);
	input->setPortNumber(pos);
}

void Node::setOutputType(std::size_t pos, BaseOutput* output)
{
	if (pos >= d_ptr->_output.size())
		return;
	
	d_ptr->_output[pos] = std::shared_ptr<BaseOutput>(output);
}

