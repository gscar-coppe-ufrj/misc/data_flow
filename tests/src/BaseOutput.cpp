#include <Dataflow/BaseOutput.h>
#include <Dataflow/BaseInput.h>

#include <boost/shared_ptr.hpp>

#include <memory>
#include <vector>

using namespace Dataflow;
using namespace std;

class BaseOutput::Impl
{
public:
    ~Impl() {
    }
	BaseSignal* _value;
	std::vector<BaseInput*> sinks_;
};

BaseOutput::BaseOutput(BaseSignal* type) : d_ptr(new Impl) 
{
	d_ptr->_value = type;
}

BaseOutput::~BaseOutput()
{
	delete d_ptr;
}

ConnectionInfo BaseOutput::connectionInfo()
{
    ConnectionInfo info;
    
    info.count = d_ptr->sinks_.size();
    for(vector<BaseInput*>::iterator it = d_ptr->sinks_.begin(); it != d_ptr->sinks_.end(); it++) {
        info.nodeId.push_back((*it)->getNodeId());
        info.port.push_back((*it)->getPortNumber());
    }
    
    return info;
}

void BaseOutput::connect(BaseInput* base)
{
	if (base->check(d_ptr->_value) == false) {
	} else {
		base->bind(d_ptr->_value);
		base->connect(this);
		//boost::shared_ptr<BaseInput> ptr(base);
		d_ptr->sinks_.push_back(base);
	}
}

const BaseSignal* BaseOutput::value() const 
{
	return d_ptr->_value;
}

void BaseOutput::set(const BaseSignal* rhs)
{
	*(d_ptr->_value) = rhs;
}

