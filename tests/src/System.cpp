#include <Dataflow/System.h>
#include <Dataflow/BaseOutput.h>

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace Dataflow;
using namespace std;

using boost::shared_ptr;
using boost::make_shared;

class System::Impl
{
public:
    Impl() : lastId(1) {};
    
    vector<Node*> nodes;
    unsigned int lastId;
};

System::System() : d_ptr(new Impl)
{
}

System::~System()
{
    delete d_ptr;
}

void System::addNode(Node* node)
{
    node->setId(d_ptr->lastId);
    d_ptr->nodes.push_back(node);
    d_ptr->lastId++;
}

void System::save()
{
    for(vector<Node*>::iterator it = d_ptr->nodes.begin(); it != d_ptr->nodes.end(); it++) {
        cout << "<Block name='" << (*it)->name() << "' id='" << (*it)->getId() << "'>\n";
        for(unsigned int i = 0; i < (*it)->outputCount(); i++) {
            unsigned int sourceId = (*it)->getId();
            
            ConnectionInfo info = (*it)->output(i).connectionInfo();
            
            for (unsigned int j = 0; j < info.count; j++) {
                unsigned int sinkId = info.nodeId[j];
                unsigned int sinkPort = info.port[j];
                
                cout << "\t<Connection source='" << sourceId << ":" << i 
                        << "' sink='" << sinkId << ":" << sinkPort << "'>\n";
            }
        }
        cout << "</Block>\n";
    }
}
