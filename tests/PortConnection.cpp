#include <Dataflow/Input.h>
#include <Dataflow/Output.h>

#define BOOST_TEST_MODULE MyTest
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>


using namespace Dataflow;
using namespace std;

class A : public BaseSignal{
public:
    virtual ~A() {};
    
    virtual BaseSignal& operator=(const BaseSignal* rhs) {
    	const A* a = dynamic_cast<const A*>(rhs);
		if (a != 0)
            v = a->v;
        return *this;
    };
    
    int v;
};

class B : public A {
public:
    virtual ~B() {};
    
    virtual BaseSignal& operator=(const BaseSignal* rhs) {
        const B* b = dynamic_cast<const B*>(rhs);
        if (b != 0) {
            v = b->v;
            w = b->w;
        }
        return *this;
    }
    
    float w;
};

BOOST_AUTO_TEST_CASE(ConnectTest)
{
    // Create an input and output of the same type A
    BaseOutput* out = new Output<A>();
    BaseInput* in = new Input<A>();
    
    // Connect the two
    out->connect(in);

    // Set the value of the output
    A a;
    a.v = 2;
    out->set(&a);
    
    // Check if the input's value has also changed
    const A* a2 = in->value<A>();
    BOOST_CHECK(a2->v == a.v);
}

BOOST_AUTO_TEST_CASE(PolymorphicConnect)
{
    BaseOutput* out = new Output<B>();
    BaseInput* in = new Input<A>();
    
    out->connect(in);
    
    B b;
    b.v = 3;
    b.w = 4.5f;
    out->set(&b);
    
    const A* a = in->value<A>();
    BOOST_CHECK(a->v == b.v);
}

BOOST_AUTO_TEST_CASE(ConnectUsingOperator)
{
    BaseOutput* out = new Output<A>();
    BaseInput* in  = new Input<A>();
    
    BaseOutput& outref = *out;
    
    outref >>= *in;
    
    B b;
    b.v = 3;
    b.w = 4.5f;
    out->set(&b);
    
    const A* a = in->value<A>();
    BOOST_CHECK(a->v == b.v);
}

