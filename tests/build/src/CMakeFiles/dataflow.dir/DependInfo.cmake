# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/admin/Documents/simulink/src/BaseInput.cpp" "/home/admin/Documents/simulink/build/src/CMakeFiles/dataflow.dir/BaseInput.cpp.o"
  "/home/admin/Documents/simulink/src/BaseOutput.cpp" "/home/admin/Documents/simulink/build/src/CMakeFiles/dataflow.dir/BaseOutput.cpp.o"
  "/home/admin/Documents/simulink/src/BaseSignal.cpp" "/home/admin/Documents/simulink/build/src/CMakeFiles/dataflow.dir/BaseSignal.cpp.o"
  "/home/admin/Documents/simulink/src/Node.cpp" "/home/admin/Documents/simulink/build/src/CMakeFiles/dataflow.dir/Node.cpp.o"
  "/home/admin/Documents/simulink/src/System.cpp" "/home/admin/Documents/simulink/build/src/CMakeFiles/dataflow.dir/System.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
