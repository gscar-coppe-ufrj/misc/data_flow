# CMake generated Testfile for 
# Source directory: /home/admin/Documents/simulink
# Build directory: /home/admin/Documents/simulink/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(tests)
SUBDIRS(src)
SUBDIRS(examples)
