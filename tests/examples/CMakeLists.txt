include_directories(${CMAKE_SOURCE_DIR}/include)

add_executable(integrator Integrator.cpp)
target_link_libraries(integrator dataflow)
